package com.sicap.psm;

import java.time.LocalDateTime;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableCaching
@EnableScheduling
@Slf4j
public class CachingConfig {
 
	public static final String INTERNET_SERVICES = "internetServices";
	
    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("internetServices");
    }
   
    @CacheEvict(allEntries = true, value = {INTERNET_SERVICES})
    @Scheduled(cron = "${fetch.internetservices.scheduler.cron}")
    public void fulshInternetServiceCache() {
    	log.info("Flush Internet service Cache " + LocalDateTime.now());
    }
}
