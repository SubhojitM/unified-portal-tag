package com.sicap.psm.portal.domain.model;

import java.io.Serializable;

import lombok.ToString;

@ToString
public class InternetServiceUPView implements Serializable {

    private static final long serialVersionUID = 5247382914272715656L;
    private String name;
    private String description;
    private String logo;
    private String dns;
}
