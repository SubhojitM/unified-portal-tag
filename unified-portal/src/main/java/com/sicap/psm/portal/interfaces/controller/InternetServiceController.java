package com.sicap.psm.portal.interfaces.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sicap.psm.portal.domain.model.GetInternetServicesRepository;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@RequestMapping("/internetservices")
public class InternetServiceController {
	
	@Value("${internetservices.page.title}")
    private String title;
	
	@RequestMapping
    public ModelAndView  getServices(ModelMap model, HttpServletRequest request) throws Exception {
		log.debug("accepted a internet service view request");
		model.addAttribute("services", getInternetServices.getServices());
		model.addAttribute("view", "fragments/servicedetails");
		return new ModelAndView("main");
	}
	
	@Inject
	private GetInternetServicesRepository getInternetServices;

}
