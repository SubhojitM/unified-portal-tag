package com.sicap.psm.portal.domain.model;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;

public interface GetInternetServicesRepository {

	@Cacheable("internetServices")
	public List<InternetServiceUPView> getServices();

}
