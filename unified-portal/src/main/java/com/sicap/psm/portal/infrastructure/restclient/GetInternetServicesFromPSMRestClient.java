package com.sicap.psm.portal.infrastructure.restclient;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.sicap.psm.portal.domain.model.GetInternetServicesRepository;
import com.sicap.psm.portal.domain.model.InternetServiceUPView;

import lombok.extern.slf4j.Slf4j;

@Named
@Slf4j
public class GetInternetServicesFromPSMRestClient implements GetInternetServicesRepository {

	@SuppressWarnings("unchecked")
	@Override
	public List<InternetServiceUPView> getServices() {		
        HttpEntity<String> request = new HttpEntity<>("parameters", httpHeaders);
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        log.debug("loaded internet services with value: " + response.getBody());
		return (List<InternetServiceUPView>)response.getBody();
  	}
	
	@Inject
	private RestTemplate restTemplate;
	@Value("${psm.rest.service.url}")
	private String url;
	@Inject
	private HttpHeaders httpHeaders;

}
