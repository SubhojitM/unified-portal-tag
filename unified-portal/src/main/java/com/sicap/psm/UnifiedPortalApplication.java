package com.sicap.psm;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@SpringBootApplication
public class UnifiedPortalApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(UnifiedPortalApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	 /**
     * This method allows to manage internalization and the current language of user by cookie
     *
     * @return LocaleResolver for internalization
     */
    @Bean
    public LocaleResolver localeResolver() {
        return new CookieLocaleResolver();
    }

    /**
     * This method allows to manage internalization and update of user language
     *
     * @return LocaleChangeInterceptor for internalization
     */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    /**
     * This method allows to manage internalization and indicates the message bundle file's location
     *
     * @return MessageSource for internalization
     */
    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:locale/messages");
        messageSource.setDefaultEncoding(UTF8);
        return messageSource;
    }
    
    @Bean
    public HttpHeaders httpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            String plainCreds = username.concat(":").concat(password);
            byte[] plainCredsBytes = plainCreds.getBytes();
            byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
            String base64Creds = new String(base64CredsBytes);
            headers.add("Authorization", "Basic " + base64Creds);
        }
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "application/json");
        return headers;
    }
    
    @Value("${psm.api.admin.username}")
	private String username;
	@Value("${psm.api.admin.password}")
	private String password;
	public static final String UTF8 = "UTF-8";
}
