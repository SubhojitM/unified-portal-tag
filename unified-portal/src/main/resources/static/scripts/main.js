/** The number of max tries did when polling for subscription creation */
var NB_TRIES = 5;

function submitAsyncForm(formId, buttonId, formUrl, pollUrl) {
    // the original HTML text is saved to be restored if it is needed
    var submitButtonText = $(buttonId).html();
    // all submit buttons are disabled
    disableSubmitButtons();
    // a loading gif is added to the subscribe button
    $(buttonId).html('<img src="/images/loading.gif" /> ' + submitButtonText);

    // the form is submitted
    $.post(formUrl, $(formId).serialize(), function (data) {
        // if the submission is successfull we wait for the item to be created
        pollItemCreation(submitButtonText, buttonId, pollUrl + '?url=' + encodeURIComponent(data.url), NB_TRIES);
    }).fail(function (data) {
        // if the form submission fails, the buttons are re-enabled
        enableSubmitButtons();
        // the button text is set to the original value (removing the loading animated gif)
        $(buttonId).html(submitButtonText);
        // an error message is displayed
        $("[role='alert']").html(data.responseJSON.message).removeClass('hidden')
    });
}

/**
 * Disables all submit buttons in the page
 */
function disableSubmitButtons() {
    $("button").each(function () {
        $(this).prop('disabled', true)
    });
}

/**
 * Enables all submit buttons in the page
 */
function enableSubmitButtons() {
    $("button").each(function () {
        $(this).prop('disabled', false)
    });
}


/**
 * Polls the provided url NB_TRIES times (the function is called recursively in case of error until the number
 * of tries is reached).
 * If an error occurred an error message is displayed.
 * If it succeeds, the location is changed to the URL provided in the response data.
 *
 * @param submitButtonText the original submit button text to be restored in case of error
 * @param id the identifier of the button to restore
 * @param url the url to poll
 * @param cpt the number of tries remaining.
 */
function pollItemCreation(submitButtonText, id, url, cpt) {
    console.log('Polling URL');
    $.get(url, function (data) {
        // if the get request is successful, the location is changed to produce a redirection
        sendRedirect(data.url);
    }).fail(function (data) {
        // burning one try
        cpt--;
        if ((data.status == 404) && (cpt > 0)) {
            // if no subscription has been found and if there is enough tries remaining, the function is re-called
            setTimeout(function(){ pollItemCreation(submitButtonText, id, url, cpt); }, 1000);
        } else {
            // in case of error, the submit button HTML text is restored to the original value
            $(id).html(submitButtonText);
            // all the submit buttons in the page are re-enabled
            enableSubmitButtons();
            // an error message is displayed
            $("[role='alert']").html(data.responseJSON.message).removeClass('hidden');
        }
    });
}

function sendRedirect(url) {
    window.location.replace(url);
}
