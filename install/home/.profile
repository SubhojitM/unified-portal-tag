#                             \\\\
#                             (. .)
#                              ( )
# ccm.sicap@sicap.com-----ooO-------Ooo--------.profile-----
#
# Sicap product - user profile
# This script should set all environment variabled needed by
# sicap products.
#
# ###########################################################
#
# ATTENTION
#
# THIS FILE IS MAINTAINED BY SICAP AND SHOULD NOT BE MODIFIED
# IN ANY WAY.
#
# PLEASE REFER TO THE CUSTOMER SPECIFIC PROFILES FOR SPECIFIC
# ENVIRONMENT SETTINGS.
#
# Sicap Ltd., Switzerland
# Mail : Hotline.SICAP@sicap.com
# Phone: +41-848-807 387
#
# ###########################################################

# define customer specific profile for non-interactive login
CUSTOMER_PROFILE_NI=".profile.customer.noninteractive"

# define customer specific profile for interactive login
CUSTOMER_PROFILE_I=".profile.customer.interactive"

# Prompt
# SSH extension
if [ $SSH_TTY ]; then
  PS1="SSH:"Sicap-$LOGNAME"@"`uname -n`$" "
else
  PS1=Sicap-$LOGNAME"@"`uname -n`$" "
fi
export PS1
BIN=/opt/unifiedportal/current/bin
export BIN
LOG_BASE=/var/opt/unifiedportal/logs
export LOG_BASE
CONF=/opt/unifiedportal/current/conf
export CONF
export PATH=${PATH}:/opt/unifiedportal/current/bin/
# source interactive or non-interactive profile depending
# on the type of the terminal
if tty -s; then

  # source customer specific profile for interactive login if
  # available
  if [ -f ./$CUSTOMER_PROFILE_I ]; then
    . ./$CUSTOMER_PROFILE_I
  fi

else

  # source customer specific profile for non-interactive login
  # if available
  if [ -f ./$CUSTOMER_PROFILE_NI ]; then
    . ./$CUSTOMER_PROFILE_NI
  fi

fi

# Change umask so that the files created have appropriate permissions
umask 002

# source sicap profile
# this profile will setup product-specific environment
if [ -f ./.profile.sicap ]; then
  . ./.profile.sicap
fi